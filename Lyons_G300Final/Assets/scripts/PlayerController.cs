﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float turnSpeed = 20f;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;

    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    public float speed = 1f;

    public int amount = 3;
    private int health = 3;
    public float MaxSpeed = .4f;
    public float MidSpeed = .35f;
    public float LowSpeed = .3f;

    Animator m_Animator;

    bool isWalking;


    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Animator = GetComponent<Animator>();

    }

    private void Update()
    {
        //changes the players speed according to the value of the health variable
        if(health == 3)
        {
            speed = MaxSpeed;
        }

        if(health == 2)
        {
            speed = MidSpeed;
        }

        if(health == 1)
        {
            speed = LowSpeed;
        }
        
        if(health <= 0)
        {

            Destroy(gameObject);
        }
    }

    void FixedUpdate()
    {
        //calls the move function
        Move();
    }

    private void Move()
    {
        //set the horizontal and vertical axis
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        //set the movement Vector
        m_Movement.Set(horizontal, 0f, vertical);

        //normalize the vector
        m_Movement.Normalize();

        //setting a bool to whether there is horizontal input or not
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);

        //setting a bool to whether there is vertical input or not
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);

        //combine the two bools
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("isWalking", isWalking);
        if (isWalking)
        {
            //moving the animation in the desired direction
            m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * speed);

            //moving rotation
            m_Rigidbody.MoveRotation(m_Rotation);
        }
        //sets the forward vector
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed *
            Time.deltaTime, 0f);

        //stores the rotation
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }
    void OnTriggerEnter(Collider other)
    {
        //when player collides with projectile subtract 1 from health
        if (other.gameObject.CompareTag("Projectile"))
        {
            health -= 1;
        }
             
    }
}