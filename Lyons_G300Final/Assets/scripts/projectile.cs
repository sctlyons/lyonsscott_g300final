﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        //projectiles destroy themselves once they hit any object that does not have the projectile tag
        if (other.gameObject.CompareTag("Projectile") == false)
        {
            Destroy(gameObject);
        }
        //destroys enemy once projectiles hits the enemy
        if (other.gameObject.CompareTag("enemy"))
        {
            Destroy(other.gameObject);
        }
    }
}
