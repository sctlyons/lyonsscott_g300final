﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public GameObject projectilePrefab;
    public Transform shotSpawn;
    public float shotSpeed = 10f;
    private PlayerController player;

    public Transform Player;

    public Transform spawnPoint;
    public float moveSpeed = 2f;

    public AudioClip clip;

    private AudioSource audio;

    void Start()
    {
        audio = gameObject.GetComponent<AudioSource>();
        //calls shoot function every .5 seconds after the first 3 seconds
        InvokeRepeating("shoot", 3f, .5f);
        
        Player = GameObject.FindWithTag("Player").transform;
    }
    //called when the game object is destroyed
    private void OnDestroy()
    {
        //sets enemies spawnpoint to active
        spawnPoint.gameObject.SetActive(true);
    }
    // Update is called once per frame
    void shoot()
    {
        //create the projectile
        GameObject projectile = Instantiate(projectilePrefab, shotSpawn.transform.position, projectilePrefab.transform.rotation);
        Rigidbody projectileRB = projectile.GetComponent<Rigidbody>();
        projectileRB.velocity = transform.forward * shotSpeed;
        audio.PlayOneShot(clip);
    }

    void Update()
    {
        //rotates the enemy to look at the player
        Vector3 lookAt = Player.position;
        lookAt.y = transform.position.y;
        transform.LookAt(lookAt);
    }
}
