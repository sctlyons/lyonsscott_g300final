﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public GameObject enemyPrefab;
    public Transform[] enemySpawns;
 

    // Start is called before the first frame update
    void Start()
    {
        //Invoke the SpawnEnemy function every 2.5 seconds after the first 3 seconds
        InvokeRepeating("SpawnEnemy", 3f, 2.5f);
    }

    void SpawnEnemy()
    {
        //chooses a random active EnemySpawn to instatiate the enemy at
        Transform enemySpawn = enemySpawns[(int)(Random.value * enemySpawns.Length)];
        if (enemySpawn.gameObject.activeSelf)
        {
            enemySpawn.gameObject.SetActive(false);
            GameObject enemy = Instantiate(enemyPrefab, enemySpawn.transform.position, enemyPrefab.transform.rotation);
            enemy.GetComponent<EnemyController>().spawnPoint = enemySpawn;
        }
   
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
